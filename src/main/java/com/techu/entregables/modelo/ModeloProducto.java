package com.techu.entregables.modelo;

import java.util.ArrayList;
import java.util.List;

public class ModeloProducto {
    private int id;
    private String marca;
    private String descripcion;
    private double precio;
    private final List<ModeloUsuario> usuarios;

    public ModeloProducto(int id, String marca, String descripcion, double precio) {
        this.id = id;
        this.marca = marca;
        this.descripcion = descripcion;
        this.precio = precio;
        this.usuarios = new ArrayList<ModeloUsuario>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<ModeloUsuario> getUsuarios() {
        return usuarios;
    }

//    public void setUsuarios(List<ModeloUsuario> usuarios) {
//        this.usuarios = usuarios;
//    }
}
