package com.techu.entregables.controlador;

import com.techu.entregables.modelo.ModeloProducto;
import com.techu.entregables.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.version}/productos")
public class ControladorProductos {

    @Autowired
    private ServicioDatos servicioDatos;

    @GetMapping
    public ResponseEntity obtenerProductos() {
        return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }

    @PostMapping
    public ResponseEntity crearProducto(@RequestBody ModeloProducto producto) {
        final ModeloProducto p = this.servicioDatos.agregarProducto(producto);
        return ResponseEntity.ok(p);
    }

    @GetMapping("/{id}")
    public ResponseEntity obtenerUnProducto(@PathVariable int id) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if(p != null) {
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity borrarUnProducto(@PathVariable int id) {
        this.servicioDatos.borrarProducto(id);
        return new ResponseEntity("¡Eliminado!", HttpStatus.NO_CONTENT);
    }
}
